#include <iostream>
#include <string>
#include <time.h>
using namespace std;


struct node {
	string data;
	node* next = NULL;
	node* previous = NULL;
};


//This is the function that generates the choice of the Lord Commander within the game!
void CommanderChoice(node* positive, node*& storage) {
	int GeneratedChoice = rand() % 5 + 1;

	cout << "As Lord Commander I choose to start at the number: ";

	for (int i = 0; i < GeneratedChoice; i++) {
		if (i == (GeneratedChoice - 1)) {
			storage = positive;
			cout << i + 1 << endl;
		}
		positive = positive->next;
	}
}


//This function generates the position of cloak when passing it!
void CloakPosition(node* positive, node* negative, node*& storage, int compute) {
	for (int i = 1; i < compute; i++) {
		if (storage == negative) {
			storage = positive;

			cout << "The Cloak has stopped, moving to the next position!" << endl;

		}
		else
			storage = storage->next;
	}
	cout << "The Cloak Has Chosen! " << endl;
	system("pause");
	system("cls");
	cout << "Soldier: " << storage->data << endl;
	cout << "Go and protect the wall soldier! " << endl;
	system("cls");

	node* temp = storage;
	if (storage == negative) { storage = positive; }
	else { storage = storage->next; }
	storage->previous = temp;

	cout << storage->previous->data << " Has joined to defend the wall!" << endl;
	system("pause");
	system("cls");
	cout << " The cloak now lies with soldier: " << storage->data << endl;
	system("pause");
	system("cls");

}


//This function creates the user input for the soldiers who will participate in the game!
void NameInput(node*& positive, node*& negative) {

	string soldier;
	cout << "Who are the Men of the Watch?" << endl;
	system("pause");
	system("cls");

	for (int i = 0; i < 5; i++) {
		node* temp = new node;


		cout << "What is ye name Soldier " << i + 1 << ": ";
		cin >> soldier;
		system("pause");
		system("cls");


		temp->data = soldier;
		temp->next = NULL;


		if (i == 0) {
			positive = temp;
			negative = temp;
		}


		else {
			negative->next = temp;
			negative = temp;
		}
	}
}


//This part of the code outputs how many of the Nights Watchmen are left!
void MembersOutput(node* positive) {

	cout << "Here lies the remaining Men of the Watch: ";

	while (positive != NULL) {
		cout << positive->data << ' ';
		positive = positive->next;
	}
	cout << endl;
	system("pause");
	system("cls");
}


//This function generates a random number based on the number of members of the Nights Watch
int ComputeRand(node* positive, node* storage) {
	int position = 0;
	int RandOutput;

	while (positive != NULL) {
		position++;
		positive = positive->next;
	}

	RandOutput = rand() % position + 1;

	cout << storage->data << " is the holder and has chosen the number: " << RandOutput << endl;

	return RandOutput;
}


int main() {
	srand(time(NULL));
	node* positive = NULL;
	node* negative = NULL;
	node* storage;
	int compute;

	cout << "Welcome to THE WALL" << endl;
	system("pause");
	system("cls");

	cout << "You are the Lord Commander and the wall is under attack!" << endl;
	cout << "As the Lord Commander you must choose whom among your men will retreat and rally troops to defend the wall." << endl;
	system("pause");
	system("cls");

	NameInput(positive, negative);
	system("cls");

	MembersOutput(positive);
	CommanderChoice(positive, storage);
	compute = ComputeRand(positive, storage);
	CloakPosition(positive, negative, storage, compute);
	MembersOutput(positive);
	system("pause");
	return 0;
}