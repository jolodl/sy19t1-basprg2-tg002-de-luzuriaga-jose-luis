#include<iostream>
#include<stdlib.h>
#include<conio.h>
using namespace std;


// DISPLAY OF ITEMS IN THE LIST
void PackageOut() {
	cout << " " << endl;
	cout << "Packages Avaialable: " << endl;
	cout << " " << endl;
	cout << "Package 1 for $150" << endl;
	cout << "Package 2 for $500" << endl;
	cout << "Package 3 for $1780" << endl;
	cout << "Package 4 for $4050" << endl;
	cout << "Package 5 for $13333" << endl;
	cout << "Package 6 for $30750" << endl;
	cout << "Package 7 for $250000" << endl;}


//CURRENT GOLD DISPLAY
void MoneyOut(int cash, int gems) {
	cout << " " << endl;
	cout << "THE CURRENT AMOUNT OF GOLD IN YOUR INVENTORY IS: " << cash << endl;
	cout << "THE CURRENT AMOUNT OF GEMS IN YOUR INVENTORY IS: " << gems << endl;
	cout << " " << endl;}


// SORTING FUNCTION
int* Sort(int bundle[]) {
	int var1 = 0;
	for (int l = 0; l < 7; l++) {
		for (int k = l + 1; k < 7; k++) {
			if (bundle[k] < bundle[l]) {
				var1 = bundle[l];
				bundle[l] = bundle[k];
				bundle[k] = var1;}}}
	return bundle;}


//IN GAME PURCHASES PRICES DISPLAY
void WalletCharge() {
	cout << " " << endl;
	cout << " Packages Available: " << endl;
	cout << " " << endl;
	cout << " Bag of Gold: $0.99 For 1000 Gold" << endl;
	cout << " Bags of Gold: $2.99 For 5000 Gold" << endl;
	cout << " Stash of Gold: $9.99 For 30000 Gold" << endl;
	cout << " Gold Vault: $49.99 For 200000 Gold" << endl;}


// PAYOUT FOR IN GAME GOLD TRANSACTIONS
int GoldTransaction(int bundle[], int cash, int gems, int command){
	string commandconfirm;
	if (command == 500){
		cout << " " << endl;
		cout << "You do not have enough gold to purchase the package you are trying to buy!" << endl;
		cout << " " << endl; 
		system("pause");
		system("cls");
		MoneyOut(cash, gems);
		WalletCharge();
		cout << " " << endl;
		cout << "Purchase: Bag of Gold for $0.99?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		cin >> commandconfirm;
		if ((commandconfirm == "Y") || (commandconfirm == "y")){
			cash = 1000 + cash;
			cout << " " << endl;
			cout << "You have successfully completed a transaction, you have now purchased the bundle! Congratulations!" << endl;
			cout << " " << endl;
			return cash;}
		else{
			return cash;}}

	else if ((command == 1780) || (command == 4050)){
		cout << " " << endl;
		cout << "You do not have enough gold to purchase the package you are trying to buy!" << endl;
		cout << " " << endl;
		system("pause");
		system("cls");
		MoneyOut(cash, gems);
		WalletCharge();
		cout << " " << endl;
		cout << "Purchase: Bags of Gold for $2.99?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		cin >> commandconfirm;
		if ((commandconfirm == "Y") || (commandconfirm == "y")){
			cash = 5000 + cash;
			cout << " " << endl;
			cout << "You have successfully completed a transaction, you have now purchased the bundle! Congratulations!" << endl;
			cout << " " << endl;
			return cash;}
		else{
			return cash;}}

	else if (command == 13333){
		cout << " " << endl;
		cout << "You do not have enough gold to purchase the package you are trying to buy!" << endl;
		cout << " " << endl;
		system("pause");
		system("cls");
		MoneyOut(cash, gems);
		WalletCharge();
		cout << " " << endl;
		cout << "Purchase: Stash of Gold for $9.99?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		cin >> commandconfirm;
		if ((commandconfirm == "Y") || (commandconfirm == "y")){
			cash = 30000 + cash;
			cout << " " << endl;
			cout << "You have successfully completed a transaction, you have now purchased the bundle! Congratulations!" << endl;
			cout << " " << endl;
			return cash;}
		else{
			return cash;}}

	else if (command == 30750){
		cout << " " << endl;
		cout << "You do not have enough gold to purchase the package you are trying to buy!" << endl;
		cout << " " << endl;
		system("pause");
		system("cls");
		MoneyOut(cash, gems);
		WalletCharge();
		cout << "Purchase: The Gold Vault for $49.99?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		cin >> commandconfirm;
		if ((commandconfirm == "Y") || (commandconfirm == "y")){
			cash = 200000 + cash;
			cout << " " << endl;
			cout << "You have successfully completed a transaction, you have now purchased the bundle! Congratulations!" << endl;
			cout << " " << endl;
			return cash;}
		else{
			return cash;}}

	else if (command == 250000) {
		cout << " " << endl;
		cout << "You do not have enough gold to purchase the package you are trying to buy!" << endl;
		cout << " " << endl;
		system("pause");
		system("cls");
		MoneyOut(cash, gems);
		WalletCharge();

		cout << "Purchase: The Gold Vault for $49.99 + (2) Stash of Gold for $19.98 ?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		cin >> commandconfirm;
		if ((commandconfirm == "Y") || (commandconfirm == "y")) {
			cash = 260000 + cash;
			cout << " " << endl;
			cout << "You have successfully completed a transaction, you have now purchased the bundle! Congratulations!" << endl;
			cout << " " << endl;
			return cash;
		}
		else {
			return cash;
		}
	}

	else{
		cout << "INVALID AMOUNT ENTERED!" << endl;
		system("pause");
		cout << "REDIRECTING TO MENU" << endl;
		return cash;}

	return cash;}


// PAYOUT FUNCTION
int payout(int bundle[], int cash, int gems, int command){
	if (command == 150){
		cout << " " << endl;
		cout << "You have purchased the package! Thank you for your transaction! ^_^ " << endl;
		cout << " " << endl;
		cash = cash - command;
		return cash;}

	else{
		GoldTransaction(bundle, cash, gems, command);
		return cash;}

	return cash;}


int main(){
	char endkey;
	int bundle[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int cash = 250, gems = 0, command;
	do{
		Sort(bundle);
		PackageOut();
		cout << endl << endl;
		MoneyOut(cash, gems);
		cout << "Enter the price of the package you would like to purchase!" << endl;
		cout << "Enter Here: ";
		cin >> command;
		payout(bundle, cash, gems, command);
		cout << endl << endl;
		system("pause");
		system("cls");
		cout << " " << endl;
		cout << "Would you like to continue purchasing a package?" << endl;
		cout << "Enter (Y) to Continue or Enter (N) to Cancel " << endl;
		cout << " " << endl;
		endkey = _getch();
		cout << endl << endl;} 
	while (endkey != 'n');
	system("pause");
	return 0;}