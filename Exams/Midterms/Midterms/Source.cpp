#include<iostream>
#include <string>
#include<stdbool.h>
#include<stdlib.h>
#include<time.h>
using namespace std;


#ifndef NODE_H
#define NODE_H


struct Node{	
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;};
#endif


//This Function Creates the 1 special (Emperor Card) and the regular civilian cards!
void GAMECARDS(Node*& positive, Node*& negative, string name) {
	Node* temp = new Node;
	temp->next = NULL;
	if (positive == NULL) {
		positive = temp;
		name = "SpecialCard";
		temp->name = name;
		negative = temp;
	}
	// Creates the 4 citizens in the game, with this you can create names for them to make it more interesting!
	cout << "Choose the names of the civilians in the Card Game: " << endl;
	for (int i = 0; i < 4; i++) {
		Node* hold = new Node;
		cout << "Citizen Card " << i + 1 << ": ";
		cin >> name;
		hold->name = name;
		hold->next = positive;
		negative->next = hold;
		negative = hold;
	}
	system("pause"); system("cls");
}


// This is the Interface part of the code which shows: Users Cash, Distance, and Round Denegatives
void Interface(int* cash, int* ContraptionDistance, string WagerBodyPart, bool SpecialCard) {
	cout << "********************************************************************" << endl;
	cout << "|Money: " << *cash << " Yen." << endl;
	cout << "|Distance from PAIN: " << *ContraptionDistance << " mm" << endl;
	cout << "********************************************************************" << endl;
	if (SpecialCard == true)
		cout << "|Side: Emperor" << endl;
	else
		cout << "|Side: Slave" << endl;}


// Function in the game that allows the user to gamble (Eardrum Contraption Distance)
int UserBet(int BetPrice) {
	cout << "Enter the distance you want to gamble? ";
	do{cin >> BetPrice;
	} while ((BetPrice > 30) || (BetPrice < 0));
	system("pause"); system("cls");
	return BetPrice;}


//This is the Payout Function of the code
int payout(int KaijiCard, int EnemyCard, bool SpecialCard, int BetPrice, int* cash, int* ContraptionDistance) {
	//DRAW! Both Civilian
	if ((KaijiCard == 0) && (EnemyCard != 0)) {
		cout << "\nDRAW AS BOTH PLAYERS ARE CIVILIANS." << endl;
		return *ContraptionDistance;}
	else if ((KaijiCard == 1) && (EnemyCard == 0)){
		//LOSS! Player is Emperor while Opponent is Slave
		if (SpecialCard == true){
			cout << "\nKAIJI HAS LOST, KAIJI IS EMPEROR AND OPPONENT IS SLAVE. MOVING CONTRAPTION " << BetPrice << " mm" << endl;
			*ContraptionDistance -= BetPrice;
			return *ContraptionDistance;}
		else if (SpecialCard == false){
			BetPrice *= 500000;
			*cash += BetPrice;
			cout << "\nKAIJI HAS WON! " << BetPrice << " Yen." << endl;
			return *cash;}}
	else if ((KaijiCard == 1) && (EnemyCard != 0)){
		//Win! Player is Emepror while Opponent is Civilian
		if (SpecialCard == true) {
			BetPrice *= 100000;
			*cash += BetPrice;
			cout << "\nKAIJI HAS WON! KAIJI IS EMPEROR AND OPPONENT IS CIVILIAN. KAIJI WINS  " << BetPrice << " Yen" << endl;
			return *cash;}
		else if (SpecialCard == false){
			cout << "\nKAIJI HAS LOST, MOVING CONTRAPTION " << BetPrice << " mm" << endl;
			*ContraptionDistance -= BetPrice;
			return *ContraptionDistance;}}
	else if ((KaijiCard == 0) && (EnemyCard == 0)){
		//LOSS! Player is Civilian while Opponent is Emperor
		if (SpecialCard == true) {
			cout << "\nKAIJI HAS LOST, KAIJI IS CIVILIAN AND OPPONENT IS EMPEROR. MOVING CONTRAPTION " << BetPrice << " mm.\n\n";
			*ContraptionDistance -= BetPrice;
			return *ContraptionDistance;}
		//Win! Player is civilian while Opponent is Slave
		else if (SpecialCard == false){
			BetPrice *= 500000;
			*cash += BetPrice;
			cout << "\nKAIJI HAS WON! KAIJI IS CIVILIAN AND OPPONENT IS SLAVE. KAIJI WINS  " << BetPrice << " Yen" << endl;
			return *cash;}}
	return *ContraptionDistance;}


void displayList(Node* positive) {
	cout << " " << endl;
	cout << "Cards in the Game: ";
	Node* C = positive;
	do{
		cout << C->name << ' ';
		C = C->next;} while (C != positive);}


//Deleting Nodes
void deleteNode(Node*& positive, Node*& negative) {
	Node* B = positive;
	Node* C = positive;
	if (positive == negative){
		delete positive;
		positive == NULL;
		negative == NULL;}
	else{
		while (C->next != positive){
			B = C;
			C = C->next;}
		B->next = C->next;
		delete C;
		C = NULL;}}


// This is the fuction that makes the rounds in the game possible 
int GameRound(Node*& positive, Node*& negative, bool SpecialCard, int BetPrice, int* cash, int* ContraptionDistance) {
	// Kaiji and Opponent Card
	int KaijiCard, EnemyCard; 
	int EnemyCardChoice = 4;
	for (int i = 0; i < 5; i++) {
		// Opponent chooses slave or civilian
		EnemyCard = rand() % EnemyCardChoice; 
		if (SpecialCard == true)
			cout << "Kaiji controls: Emperor card!" << endl;
		else if (SpecialCard == false)
		cout << "Kaiji controls: Slave card!" << endl;
		displayList(positive);
		cout << " " << endl;
		cout << "*****************************************************************************************" << endl;
		cout << "|Civilian or Special card (Emperor or Slave)!" << endl;
		cout << "|Enter 0 to get a civilian card // Enter 1 to get the Emperor or Slave card: ";
		cin >> KaijiCard;
		cout << " " << endl;
		cout << "*****************************************************************************************" << endl;
		system("pause"); system("cls");
		if (*ContraptionDistance < 0) { break; }
		if (SpecialCard == true) { cout << "Kaiji controls: Emperor card!"<< endl; }
		else if (SpecialCard == false) { cout << "Kaiji controls: Slave card!"<< endl; };

		if (EnemyCard == 0){
			if (SpecialCard == true)
				cout << "Yukio Tonegawa has chosen: Slave card" << endl;
			else
				cout << "Yukio Tonegawa has chosen: Emperor card" << endl;}
		else if (EnemyCard != 0){
			cout << "Yukio Tonegawa has chosen: CIVILIAN Card!" << endl;}
		if (KaijiCard == 0){
			cout << "Kaiji has chosen: CIVILIAN Card!" << endl;
			if ((SpecialCard == true) && (EnemyCard == 0)) {
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}
			else if (EnemyCard != 0)   {
				deleteNode(positive, negative);}
			else if ((SpecialCard == false) && (EnemyCard == 0)){
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}}
		else if (KaijiCard == 1){
			if ((SpecialCard == true) && (EnemyCard != 0)){
				cout << "Kaiji has chosen: Emperor card!" << endl;
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}
			else if ((SpecialCard == false) && (EnemyCard == 0)){
				cout << "Kaiji has chosen: Slave card!" << endl;
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}
			else if ((SpecialCard == false) && (EnemyCard != 0)){
				cout << "Kaiji has chosen: Slave card!" << endl;
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}
			else if ((SpecialCard == true) && (EnemyCard == 0)){
				cout << "Kaiji has chosen: Emperor card!" << endl;
				payout(KaijiCard, EnemyCard, SpecialCard, BetPrice, cash, ContraptionDistance);
				break;}}if (*ContraptionDistance < 0) { break; }

		system("pause"); system("cls");
		EnemyCardChoice--;}
	return *ContraptionDistance;}


//Ending Output of Emperor Card
void evaluateEnding(int* ContraptionDistance, int* cash, string WagerBodyPart) {
	system("pause");
	system("cls");
	//ABSOLUTE WINNER ENDING
	if ((*ContraptionDistance > 0) && (*cash >= 20000000)) {
		cout << "YOU WIN! YOU ARE AN ABSOLUTE WINNER CONGRATULATIONS!" << endl;
		system("pause");
		cout << "The Emperor Card Game has ended, Kaiji Wins!" << endl;
		system("pause");
		system("cls");
	}
	//WINNING ENDING
	else if ((*ContraptionDistance > 0) && (*cash < 20000000)) {
		cout << "The Emperor Card Game has ended, Kaiji Wins!" << endl;
		system("pause");
		cout << "Kaiji has a remaining of: " << *cash << endl;
		system("pause");
		system("cls");
	}
	//LOSS OR PAINFUL ENDING
	else if (*ContraptionDistance < 0) {
		cout << WagerBodyPart << " is heavily bleeding!" << endl;
		system("pause");
		cout << "You're trying to remain conscious, but the bleeding is too much" << endl;
		system("pause");
		cout << "The Emperor Card Game has ended, you died :-(" << endl;
		system("pause");
		system("cls");}}

int main(){
	cout << "|--------------------------|" << endl;
	cout << "| Welcome to Emperor Card! |" << endl;
	cout << "|--------------------------|" << endl;
	system("pause");
	system("cls");
	srand(time(NULL));
	int BetPrice = 0;
	int* cash = new int;
	*cash = 0;
	int* ContraptionDistance = new int;
	*ContraptionDistance = 30;
	string WagerBodyPart;
	string name;
	Node* positive = NULL;
	Node* negative = NULL;
	bool SpecialCard = true;
	int round = 1;


	// Intro or Starting Dialougue
	cout << "You Kaiji are currently in debt, and you want to play with what money?" << endl;
	system("pause");
	cout << "Your wager if you choose to accept it is..."<< endl;
	system("pause");
	system("cls");
	cout << "AN EYE OR AN EAR!" << endl;
	cout << "HA! HA! HA! HA! HA!" << endl;
	system("pause");
	system("cls");
	cout << "Good Luck And Welcome to Emperor Card!" << endl;
	cout << "Select your Wager of Choice (Eye or Ear): ";
	cin >> WagerBodyPart; cout << endl;
	system("pause"); system("cls");
	do{
		for (int i = 1; i <= 4; i++) {
			if (round % 2 == 1){
				for (int i = 0; i < 3; i++) {
					Interface(cash, ContraptionDistance, WagerBodyPart, SpecialCard);
					GAMECARDS(positive, negative, name);
					SpecialCard = true;
					BetPrice = UserBet(BetPrice);
					GameRound(positive, negative, SpecialCard, BetPrice, cash, ContraptionDistance);
					Interface(cash, ContraptionDistance, WagerBodyPart, SpecialCard);
					system("pause"); system("cls");}}
			else if (round % 2 == 0){
				for (int i = 0; i < 3; i++){
					Interface(cash, ContraptionDistance, WagerBodyPart, SpecialCard);
					GAMECARDS(positive, negative, name);
					SpecialCard = false;
					BetPrice = UserBet(BetPrice);
					GameRound(positive, negative, SpecialCard, BetPrice, cash, ContraptionDistance);
					Interface(cash, ContraptionDistance, WagerBodyPart, SpecialCard);
					system("pause"); system("cls");}}
			round++;}} 
	while ((*ContraptionDistance < 0) || (round == 4));
	evaluateEnding(ContraptionDistance, cash, WagerBodyPart);
	system("pause");
	return 0;}