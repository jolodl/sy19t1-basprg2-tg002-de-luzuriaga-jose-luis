//Ex 3-1 (Arrays as pointer)
#include<iostream>
#include<time.h>
using namespace std;

//Function of generating the array
int GeneratedValue(int* Value){
	for (int i = 0; i < 10; i++) {
		Value[i] = rand() % 100 + 1;}
	return *Value;}

//Main
int main() {
	srand(time(NULL));
	int Value[10];

	GeneratedValue (Value);
	for (int i = 0; i < 10; i++) {
		cout << Value[i] << " ";}

	system("pause");
	return 0;}